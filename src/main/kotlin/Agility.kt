/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */

class Agility {


    fun biggerThan(numA: String, numB:String):Boolean
    {
       if(numA > numB){
           return true
       }
        return false
    }

    fun order(numA:Int, numB:Int, numC:Int, numD:Int, numE:Int): List<Int?>
    {
        var myOrderedList= listOf(numA,numB,numC,numD,numE)
       myOrderedList = myOrderedList.sorted()
        return myOrderedList
    }

    fun smallerThan(list: List<Double>): Double{
      var list = list.sorted()
        return list.first()
    }
//Palindrome number is called in Spanish capicúa
    //The number is palindrome

    fun palindromeNumber(numA: Int): Boolean
    {
        var isPalindromeNumber = false
        var sum = 0
        var Num = numA
        while (Num > 0) {
            val r = Num % 10
            sum = sum * 10 + r
            Num /= 10
        }
        if (sum == numA) {
            isPalindromeNumber = true
        }
        return isPalindromeNumber
    }
    //the word is palindrome?
    fun palindromeWord(word: String): Boolean
    {
        val sb = StringBuilder(word)
        val reverseStr = sb.reverse().toString()
        return word.equals(reverseStr, ignoreCase = true)
        return false
    }


    fun factorial(numA: Int):Int
    {
        var factorial: Int = 1
        for (i in 1..numA) {
            factorial *= i.toInt()
        }
        return factorial
    }

    //is the number odd
    fun is_Odd(numA: Byte): Boolean {
       var A : Int
        A= numA.toInt()
        if(A<0){
            A= A*-1
        }
        if ( A % 2 == 0 ){
          return false
        }else{return true}
    }

    //is the number prime
    fun isPrimeNumber(numA:Int): Boolean
    {
        var flag = false
        if(numA<0){
                 return false
             }
        for (i in 2..numA / 2) {
            if (numA % i == 0) {
                flag = true
                break
            }
        }
        if (!flag)
           return true
        else
          return false
    }

    fun is_Even(numA: Byte): Boolean
    {
        var b : Int
        b= numA.toInt()
        if(b<0){
            b= b*-1
        }
        if ( b % 2 == 0 ){
            return true
        }else{return false}
    }
    //is the number perfect
    fun isPerfectNumber(numA:Int): Boolean
    {
        var temp = 0
        for (i in 1..numA / 2) {
            if (numA % i == 0) {
                temp += i
            }
        }
        if (temp == numA) {
            return true
        } else {
            return false
        }
    }
    //Return an array with the fibonacci sequence for the requested number
    fun fibonacci(numA: Int): List<Int?>
    {
        var fib = 0
        var aux = 1
        val fibo=mutableListOf<Int>()
        if(numA > 0) {
            (0 .. numA).forEach {
                fibo.add(fib)
                aux += fib
                fib = aux - fib
            }
        }
       return  fibo
    }
    //how many times the number is divided by 3
    fun timesDividedByThree(numA: Int):Int
    {
        var cont = 0
        var aux = numA
        while(aux >= 3){
            cont=cont+1
          aux = aux-3
        }
        return cont
    }

    //The game of fizzbuzz
    fun fizzBuzz(numA: Int):String?
    {
        if ( numA%15 == 0) {
            return ("FizzBuzz")}
        else if(numA%5 == 0) {
            return ("Buzz")}
        else if(numA%3 == 0){
           return ("Fizz")}
        else {
            return numA.toString()
        }
        /**
         * If number is divided by 3, show fizz
         * If number is divided by 5, show buzz
         * If number is divided by 3 and 5, show fizzbuzz
         * in other cases, show the number
         */
    }

}