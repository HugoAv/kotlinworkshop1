/**
 * @author sigmotoa
 */
import java.lang.IllegalArgumentException
//import kotlin.IllegalArgumentException
import kotlin.math.*


class Geometric {
    //Calculate the area for a square using one side
    fun squareArea(side: Int): Int {
        val result = side * 4
        return result
    }

    //Calculate the area for a square using two sides
    fun squareArea(sideA: Int, sideB: Int): Int {
        val result = sideA * sideB
        return result
    }

    //Calculate the area of circle with the radius
    fun circleArea(radius: Double): Double {
        val PI: Double = 3.141592653589793
        return (radius * radius) * PI
    }

    //Calculate the perimeter of circle with the diameter
    fun circlePerimeter(diameter: Int): Double {
        val PI: Double = 3.141592653589793
        return (diameter * PI)
    }

    //Calculate the perimeter of square with a side
    fun squarePerimeter(side: Double): Double {
        return side * 4
    }

    //Calculate the volume of the sphere with the radius
    fun sphereVolume(radius: Double): Double {
        val PI: Double = 3.141592653589793
        val calc =Math.pow(radius,3.0)
        var resul= (4*PI*calc)/3

        return resul

    }

    //Calculate the area of regular pentagon with one side
    //Calculate the area of regular pentagon with one side
    fun pentagonArea(side: Int): Float {
        val resul = ((sqrt(5.00 * (5.00 + 2.00 * (sqrt(5.0).toFloat()))).toFloat() * side.toFloat() * side.toFloat()) / 4.00).toFloat()
        val Re = (Math.round(resul*10.0)/10.0).toFloat()
        return Re
    }

    //Calculate the Hypotenuse with two legs
    fun calculateHypotenuse(legA: Double, legB: Double): Double {
        return hypot(legA,legB)
    }
}