import java.math.BigDecimal

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
class Convertion {
    fun kmToM1(km: Double): Int {
        var R = 0
        R = (km * 1000).toInt()
        return R
    }

    //Km to metters
    fun kmTom(km: Double): Double {
        var R = 0.0
        R = km * 1000.0
        return R
    }


    fun kmTocm(km: Double): Double {
        var R = 0
        R = ((km / 0.000010000).toInt())
        return R.toDouble()
    }


    fun mmTom(mm: Int): Double {
        var R = 0.0
        R = mm / 1000.0
        return R
    }


    fun milesToFoot(miles: Double): Double {
        var R = 0
        R = ((miles * 5280 ).toInt())
        return R.toDouble()
    }

    //convert yards to inches
    fun yardToInch(yard: Int): Int {
        var R = 0
        R = (yard * 3)
        return R
    }

    //convert inches to miles
    fun inchToMiles(inch: Double): Double {
        var R = 0.0
        R = inch / 63360.0
        return R
    }

    //convert foot to yards
    fun footToYard(foot: Int): Int {
        var R = 0
        R = ((foot * 0.33333).toInt())
        return R
    }


    fun kmToInch(km: String?): Double {
        val num = km?.toDouble()
        var R = num!! * 39370.078740157
        return R

    }

    //convert milimmeters to foots
    fun mmToFoot(mm: String?): Double {
        var num = mm?.toDouble()
        var  A = (num!! / 305)
        return A
    }

    //convert yards to cm
    fun yardToCm(yard: String?): Double {
        var num = yard?.toDouble()
        var  A = (num!! * 91.44)

        return A
    }
}